---
title: "About"
menu:
  main:
    weight: 1
---

Hi :wave:

I'm an undergraduate student majoring in Informatics Engineering from Indonesian Computer University. I have experience in Software Engineer (Android and Backend Developer). I can adapt quickly and learn new technologies (tech stack/etc) and have a high curiosity. I worked on a few projects software on the campus through UNIKOM CodeLabs.
